CC = gcc
CFLAGS = -Wall -pedantic -ggdb
LDFLAGS = -lpthread -ggdb
OBJECTS = main.o network.o util.o cmd.o hash.o serfur2.o parser.o shell.o system_cmd.o control_cmd.o lua_cmd.o
INCLUDES = -Iheaders
EXECUTABLE = serfur2
BUILDDIR = build/
SRCDIR = src/

all: $(OBJECTS) main.o
	$(CC) $(addprefix $(BUILDDIR), $(OBJECTS)) $(LDFLAGS) -o $(EXECUTABLE)

%.o: $(SRCDIR)%.c
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $(BUILDDIR)$@

clean:
	rm -rf serfur2 build test_serfur2
