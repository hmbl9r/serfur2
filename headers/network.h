#ifndef NETWORK_H
#define NETWORK_H

#include "errors.h"

struct FILE;

typedef struct connection {
    int socket;
    unsigned char error;
    unsigned char connected;
} connection_t;

connection_t * net_create();
void net_destroy(connection_t *);
void net_connect_to(connection_t *, const char *, const int);
ssize_t net_send_to(connection_t *, const char *, size_t);
ssize_t net_receive_from(connection_t *, char *, size_t);
void net_disconnect_from(connection_t *);

void net_get_last_error(connection_t *, FILE *, int);

#endif
