#ifndef HASH_H
#define HASH_H

#define CMD_HASH_TYPE 0
#define CMD_HANDLE_HASH_TYPE 1

size_t hash_put(size_t, void *);
void * hash_get(size_t);
void hash_del(size_t);
size_t hash_len();
size_t hash_len_for_type(size_t);
void hash_ids_for_type(size_t, size_t *);
int hash_error();

#endif
