#ifndef PARSER_H
#define PARSER_H

#include "cmd.h"

struct FILE;

void parser_set_error_stream(FILE *);
void parse(const char *, cmd_t *);

#endif
