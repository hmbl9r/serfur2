#ifndef CMD_H
#define CMD_H

#include <stdio.h>
#include <pthread.h>

#include "util.h"
#include "errors.h"

#define CMD_BUF_SIZE 512

#define CMD_NONE 0
#define CMD_LUA 1
#define CMD_CONTROL 2
#define CMD_SYSTEM 3

struct shell;

typedef struct cmd_ret {
    unsigned char status;
    unsigned char error;
    char * data;
} cmd_ret_t;

typedef struct cmd {
    unsigned char type;
    unsigned char async;
    unsigned char store;
    unsigned char error;
    unsigned char ran;
    size_t after;
    size_t times;
    size_t callback;
    size_t id;
    size_t target;
    char * cmd_str;
    exec_handle_t * handle;
    cmd_ret_t * ret;
} cmd_t;

cmd_t * cmd_create();
void cmd_destroy(cmd_t *);
size_t cmd_len(const cmd_t *);
void cmd_dump(const cmd_t *, char *);
void cmd_load(cmd_t *, const char *);
void cmd_pretty_print(char *, const cmd_t *);
void cmd_exec(const char *, struct shell *);

#endif
