#ifndef CONTROL_CMD_H
#define CONTROL_CMD_H

#include "cmd.h"

void control_cmd_exec(cmd_t *);

#endif