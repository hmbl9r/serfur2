#ifndef SHELL_H
#define SHELL_H

#include "cmd.h"
#include "util.h"

struct FILE;

typedef struct shell {
    unsigned char looping;
    unsigned long id;
    FILE * i_stream;
    FILE * o_stream;
    cmd_t * cur_cmd;
    exec_handle_t * handle;
} shell_t;

shell_t * shell_create();
void shell_destroy(shell_t *);
void shell_start(shell_t *);
void shell_stop(shell_t *);

#endif
