#ifndef UTIL_H
#define UTIL_H

#include <pthread.h>

#include "errors.h"

#define MUTEX_LOCK(m, e) \
    if (pthread_mutex_lock(&m)) e = MUTEX_COULD_NOT_LOCK;

#define MUTEX_UNLOCK(m, e) \
    if (pthread_mutex_unlock(&m)) e = MUTEX_COULD_NOT_UNLOCK;

typedef void *(* funcptr)(void *);

typedef struct exec_handle {
    unsigned char status;
    unsigned char error;
    pthread_t thread;
    pthread_mutex_t mutex;
} exec_handle_t;

void trim(char *);
int streq(const char *, const char *);

void execute_after(int, funcptr, void *, exec_handle_t *);
void execute_every(int, int, funcptr, void *, exec_handle_t *);

void stop_execution(exec_handle_t *);

#endif
