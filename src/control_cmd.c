#include <stdlib.h>
#include <string.h>

#include "control_cmd.h"
#include "system_cmd.h"
#include "lua_cmd.h"
#include "util.h"
#include "hash.h"

#define CHECK_CMD_EXISTS(c1, c2) \
    if (!c1) { \
        add_ret_msg(c2, "command not found"); \
        return; \
    }

static void add_ret_msg(cmd_t *, const char *);
static void run(cmd_t *);
static void print(cmd_t *);
static void delete(cmd_t *);
static void stored(cmd_t *);

static void add_ret_msg(cmd_t * cmd, const char * str) {
    if (!cmd->ret) {
        cmd->ret = (cmd_ret_t *) malloc(sizeof(cmd_ret_t));
    }

    cmd->ret->data = (char *) malloc(strlen(str));
    strcpy(cmd->ret->data, str);
}

static void run(cmd_t * cmd) {
    cmd_t * cmd_to_run = (cmd_t *) hash_get(cmd->target);

    CHECK_CMD_EXISTS(cmd_to_run, cmd);

    switch (cmd_to_run->type) {
    case CMD_SYSTEM:
        system_cmd_exec(cmd_to_run);
        break;
    case CMD_CONTROL:
        control_cmd_exec(cmd_to_run); 
        break;
    case CMD_LUA:
        lua_cmd_exec(cmd_to_run);
        break;
    case CMD_NONE:
        add_ret_msg(cmd, "something bad happened");
        break;
    }
}

static void print(cmd_t * cmd) {
    cmd_t * cmd_to_print = (cmd_t *) hash_get(cmd->target);
    char buf[CMD_BUF_SIZE] = "command is too long. you should delete it!";

    CHECK_CMD_EXISTS(cmd_to_print, cmd);

    if (cmd_len(cmd_to_print) <= CMD_BUF_SIZE) {
        cmd_pretty_print(buf, cmd_to_print);
    }

    add_ret_msg(cmd, buf);
}

static void delete(cmd_t * cmd) {
    cmd_t * cmd_to_destroy = (cmd_t *) hash_get(cmd->target);

    CHECK_CMD_EXISTS(cmd_to_destroy, cmd);

    if (cmd == cmd_to_destroy) {
        add_ret_msg(cmd, "command cannot delete itself");
        return;
    }

    cmd_destroy(cmd_to_destroy);
    add_ret_msg(cmd, "command deleted");
}

static void stored(cmd_t * cmd) {
    cmd_t * cur_cmd;

    size_t len = hash_len_for_type(CMD_HASH_TYPE);
    size_t * ids = (size_t *) malloc(sizeof(size_t) * len);
    size_t stored_str_len, i, total_stored_len = 0;
    size_t so_far = 0;

    char * cmd_name;
    char * full_stored;
    char ** stored_strs = (char **) malloc(len * sizeof(char *));
    char itoa_buf[20]; /* just because of buffer overflow paranoia */

    hash_ids_for_type(CMD_HASH_TYPE, ids);

    for (i = 0; i < len; ++i) {
        cur_cmd = (cmd_t *) hash_get(ids[i]);

        switch (cur_cmd->type) {
        case CMD_SYSTEM:  cmd_name = "SYSTEM";  break;
        case CMD_CONTROL: cmd_name = "CONTROL"; break;
        case CMD_LUA:     cmd_name = "LUA";     break;
        case CMD_NONE:    cmd_name = "NONE";    break;
        }

        sprintf(itoa_buf, "%lu", ids[i]);

        stored_str_len = strlen(itoa_buf) + strlen(cmd_name) +
                          strlen(cur_cmd->cmd_str) + 7;

        stored_strs[i] = (char *) malloc(stored_str_len);

        sprintf(stored_strs[i], "[%s, %s]: %s\n", itoa_buf,
                cmd_name, cur_cmd->cmd_str);

        total_stored_len += stored_str_len;
    }

    full_stored = (char *) malloc(total_stored_len);

    for (i = 0; i < len; ++i) {
        so_far += sprintf(&full_stored[so_far], "%s", stored_strs[i]);
        free(stored_strs[i]);
    }

    add_ret_msg(cmd, full_stored);

    free(stored_strs);
    free(full_stored);
}

static const char * command_strs[] = {
    "run", "print", "delete", "stored"
};

void (* const command_funcs[])(cmd_t *) = {
    run, print, delete, stored
};

void control_cmd_exec(cmd_t * cmd) {
    size_t i;

    for (i = 0; i < 4; ++i) {
        if (streq(cmd->cmd_str, command_strs[i])) {
            command_funcs[i](cmd);
        }
    }
}