#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "hash.h"
#include "util.h"
#include "errors.h"

#define REALLOC_FACTOR 2

typedef struct item {
    size_t type;
    void * data;
} item_t;

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int error;

item_t ** items = 0;
size_t len = 3;
size_t cur_id = 0;

static void cleanup();

static void cleanup() {
    free(items);
}

size_t hash_put(size_t type, void * data) {
    size_t is = sizeof(item_t), i;

    MUTEX_LOCK(mutex, error);
    
    if (!items) {
        items = (item_t **) malloc(is * len);
        for (i = 0; i < len; ++i) {
            items[i] = 0;
        }
        atexit(cleanup);
    }
    
    if (cur_id == len) {
        items = (item_t **) realloc(items, is * len * REALLOC_FACTOR);
        for (i = len; i < len * REALLOC_FACTOR; ++i) {
            items[i] = 0;
        }
        len *= REALLOC_FACTOR;
    }

    items[cur_id] = (item_t *) malloc(is);
    
    items[cur_id]->type = type;
    items[cur_id]->data = data;
    
    MUTEX_UNLOCK(mutex, error);
    
    return cur_id++;
}

void * hash_get(size_t id) {
    void * data = 0;
    
    if (id < len) {
        MUTEX_LOCK(mutex, error);
        
        if (items[id]) {
            data = items[id]->data;
        } 
        
        MUTEX_UNLOCK(mutex, error);
    }
    
    return data;
}

void hash_del(size_t id) {    
    if (id < len) {
        MUTEX_LOCK(mutex, error);
        
        free(items[id]);
        items[id] = 0;
        
        MUTEX_UNLOCK(mutex, error);
    }
}

size_t hash_len() {
    return len;
}

size_t hash_len_for_type(size_t type) {
    size_t i, length = 0;

    MUTEX_LOCK(mutex, error);
    for (i = 0; i < len; ++i) {
        if (items[i] && items[i]->type == type) {
            ++length;
        }
    }
    MUTEX_UNLOCK(mutex, error);

    return length;
}

void hash_ids_for_type(size_t type, size_t * ids) {
    size_t i, j = 0;;

    MUTEX_LOCK(mutex, error);
    for (i = 0; i < len; ++i) {
        if (items[i] && items[i]->type == type) {
            ids[j++] = i;
        }
    }
    MUTEX_UNLOCK(mutex, error);
}

int hash_error() {
    return error;
}
