#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <netdb.h>
#include <unistd.h>

#include "util.h"
#include "network.h"

#define NOT_CONNECTED -42

static const char * error_str[] = {
    "Error: could not creating socket.",
    "Error: could not look up host.",
    "Error: could not connect to host.",
    "Error: could not send data.",
    "Error: could not receive data.",
    "not connected to a host.",
};

connection_t * net_create() {
    return (connection_t *) malloc(sizeof(connection_t));
}

void net_destroy(connection_t * conn) {
    free(conn);
    conn = 0;
}

void net_connect_to(connection_t * conn, const char * host, const int port) {
    struct sockaddr_in sa;
    struct hostent * serv = 0;
    
    assert(conn);
    assert(*host);
    assert(port > 0);
    
    if (conn->socket)
        net_disconnect_from(conn);
    
    conn->error = NET_ERROR_NONE;
    
    conn->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (conn->socket < 0) {
        conn->error = NET_CREATE_SOCKET_ERROR;
        return;
    }
    
    serv = gethostbyname(host);
    if (!serv) {
        conn->error = NET_DNS_ERROR;
        return;
    }
    
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    
    memcpy(serv->h_addr, &sa.sin_addr.s_addr, serv->h_length);
    sa.sin_port = htons(port);
    
    if (connect(conn->socket, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
        conn->error = NET_CONNECTION_ERROR;
        return;
    }
    
    if (conn->error == NET_ERROR_NONE)
        conn->connected = 1;
    else
        conn->connected = 0;
}

ssize_t net_send_to(connection_t * conn, const char * msg, size_t len) {
    ssize_t r;
    
    if (conn->connected) {
        r = write(conn->socket, msg, len);
        
        if (r < 0)
            conn->error = NET_SEND_ERROR;
    } else {
        r = 0;
        conn->error = NET_NOT_CONNECTED_ERROR;
    }
        
    return r;
}

ssize_t net_receive_from(connection_t * conn, char * buf, size_t len) {
    ssize_t r;
    
    if (conn->connected) {
        memset(buf, 0, len);
        r = read(conn->socket, buf, len);
        if (r < 0)
            conn->error = NET_RECEIVE_ERROR;
    } else {
        r = 0;
        conn->error = NET_NOT_CONNECTED_ERROR;
    }
    
    return r;
}

void net_disconnect_from(connection_t * conn) {
    close(conn->socket);
    conn->connected = 0;
}

void net_get_last_error(connection_t * conn, FILE * stream, int abort_on_fail) {    
    if (stream && conn->error != NET_ERROR_NONE) {
        if (conn->error > 1 || conn->error < 7) {
            fprintf(stream, "Invalid error code.\n");
            return;
        }
        fprintf(stream, "%s\n", error_str[-conn->error - 1]);
    }
    
    if (abort_on_fail) {
        assert(conn->error == NET_ERROR_NONE);
    }
}
