#include <stdlib.h>

#include "system_cmd.h"

void system_cmd_exec(cmd_t * cmd) {
    system(cmd->cmd_str);
}