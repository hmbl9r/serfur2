#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "serfur2.h"
#include "parser.h"
#include "cmd.h"
#include "hash.h"
#include "network.h"
#include "shell.h"

int main(int argc, char ** argv) {
    /*cmd_t * cmd;
    char buf[1024];
    size_t count = 0;
    
    parser_set_error_stream(stderr);
    
    do {
        cmd = cmd_create();
        count = 0;
        printf("serfur2> ");
        while ((buf[count++] = getc(stdin)) && buf[count - 1] != '\n');
        buf[count - 1] = 0;
        parse(buf, cmd);
        printf("input: \"%s\"\n\tis %s\n", buf, cmd->error == CMD_ERROR_NONE ? "valid" : "invalid");
        cmd_destroy(cmd);
    } while (!streq(buf, "quit"));*/
    
    shell_t * shell = shell_create();
    
    shell->i_stream = stdin;
    shell->o_stream = stdout;
    
    shell_start(shell);
    shell_stop(shell);
    
    shell_destroy(shell);
    
    /*int * ints[100];
    size_t keys[100];
    int i;
    
    for (i = 0; i < 100; ++i) {
        ints[i] = malloc(sizeof(int));
        *ints[i] = i;
        keys[i] = hash_put(ints[i]);
    }
    printf("len = %lu\n", hash_len());
    
    for (i = 0; i < 100; ++i) {
        printf("%i ", *((int *) hash_get(keys[i])));
    }
    
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    hash_put(malloc(100));
    
    printf("len = %lu\n", hash_len());*/
    
    return 0; 
}
