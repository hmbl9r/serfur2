#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "serfur2.h"
#include "network.h"
#include "util.h"
#include "cmd.h"
#include "hash.h"
#include "errors.h"

#define HOSTS_NUM 10
#define DEFAULT_PORT 45888

static const char * hmbl9r = "hmbl9r.net";

void found_online_host(char * host) {

}

void * ping_hosts(void * args) {
    connection_t * conn;
    char host_buf[32];
    int i;
    
    for (i = 0; i < HOSTS_NUM; ++i) {
        conn = (connection_t *) malloc(sizeof(struct connection));
        sprintf(host_buf, "h%i.%s", i, hmbl9r);
        
        net_connect_to(conn, host_buf, DEFAULT_PORT);
        
        if (conn->error == NET_ERROR_NONE) {
            found_online_host(host_buf);
        } else {
            free(conn);
        }
    }
    return 0;
}

void serfur2_start() {
    exec_handle_t * ping_handle =
        (exec_handle_t *) malloc(sizeof(struct exec_handle));
    
    execute_every(10, 0, ping_hosts, 0, ping_handle);
    
    while (1)
        sleep(1);
}
