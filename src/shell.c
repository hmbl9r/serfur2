#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <pthread.h>

#include "shell.h"
#include "hash.h"
#include "cmd.h"
#include "parser.h"

#define SERFUR2_PROMPT "serfur2> "

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int error;

static void * shell_loop(void *);

static void * shell_loop(void * args) {
    shell_t * shell = (shell_t *) args;
    size_t bytes_read = 0;
    
    char cmd_buf[CMD_BUF_SIZE];
    char c;
    
    while (shell->looping) {
        memset(cmd_buf, 0, CMD_BUF_SIZE);
        fprintf(shell->o_stream, SERFUR2_PROMPT);
        
        while((c = getc(shell->i_stream)) != '\n' && c)
            cmd_buf[bytes_read++] = c;
        
        if (bytes_read > CMD_BUF_SIZE) {
            error = CMD_TOO_LONG;
            break;
        }
        
        if (!bytes_read)
            continue;
        
        cmd_buf[bytes_read] = 0;
        bytes_read = 0;

        cmd_exec(cmd_buf, shell);
    }
    
    return 0;
}

shell_t * shell_create() {
    shell_t * shell = (shell_t *) malloc(sizeof(shell_t));
    
    shell->looping = 0;
    shell->id = 0;
    shell->i_stream = 0;
    shell->o_stream = 0;
    shell->cur_cmd = 0;
    shell->handle = (exec_handle_t *) malloc(sizeof(exec_handle_t));
    
    return shell;
}

void shell_destroy(shell_t * shell) {
    free(shell->handle);
    free(shell);
    
    shell = 0;
}

void shell_start(shell_t * shell) {
    shell->looping = 1;
    /*execute_after(0, shell_loop, shell, shell->handle);*/
    shell_loop(shell);
}

void shell_stop(shell_t * shell) {
    shell->looping = 0;
    stop_execution(shell->handle);
}
