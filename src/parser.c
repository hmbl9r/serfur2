#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include "parser.h"
#include "util.h"

#define CMD_BUF_SIZE 512
#define NUM_KEYWORDS 9
#define NUM_SYMBOLS 4

#define MUST_BEGIN_WITH_TOKEN \
    "must begin with exactly one of \"lua\", \"control\", or \"system\""

#define MUST_NOT_BE_LAST_TOKEN \
    "must not be the last token"

#define MUST_BE_FOLLOWED_BY_COMMA_CLOSING_BRACKET \
    "must be followed by either a comma, or a right bracket"

#define MUST_BE_FOLLOWED_BY_EQUALS \
    "must be followed by an equals"

#define MUST_BE_FOLLOWED_BY_RIGHT_BRACKET \
    "must be followed by a right bracket"

#define MUST_BE_FOLLOWED_BY_LEFT_BRACKET_OR_ARGS \
    "must be followed by a right bracket or a list of valid arguments"

#define MUST_BE_FOLLOWED_BY_COMMAND \
    "must be followed by a command"

#define MUST_BE_FOLLOWED_BY_NUMBER \
    "must be followed by a number"

#define MUST_BE_FOLLOWED_BY_ARGS \
    "must be followed by an argument"

#define DUPLICATE_PARAMETER \
    "is a duplicate parameter"

/* NOTE: commands with the async attribute always return an id
 *
 * lua(async) print('hi')
 * #=> 1
 * lua(async, store) print('bye')
 * #=> 2
 * lua(async, callback = 2) print('callback')
 * #=> 3
 * lua(after = 4) print('after')
 * lua(after = 4, times = 5) print('times')
 * lua print('Hello, world!')
 * 
 * system(async) ls -a
 * #=> 4
 * 
 * system(async, store) find .
 * #=> 5
 * 
 * control(target = 5) run
 * control(after = 4, target = 5) stop
 */

typedef enum {
    TOKEN_NONE = 0,
    LUA,
    CONTROL,
    SYSTEM,
    RIGHT_BRACKET,
    LEFT_BRACKET,
    NUMBER,
    COMMA,
    EQUALS,
    ASYNC,
    STORE,
    AFTER,
    TIMES,
    TARGET,
    CALLBACK,
    COMMAND
} token_id;

struct token {
    token_id type;
    void * data;
    struct token * next;
    struct token * prev;
};

typedef struct token token_t;

static const char * keyword_strs[] = {
    "lua", "control", "system", "async", "store", "after", "times",
    "target", "callback"
};

static const token_id keyword_ids[] = {
    LUA, CONTROL, SYSTEM, ASYNC, STORE, AFTER, TIMES, TARGET, CALLBACK
};

static const char symbols[] = {
    '(', ')', ',', '='
};

static const token_id symbol_ids[] = {
    RIGHT_BRACKET, LEFT_BRACKET, COMMA, EQUALS
};

static FILE * error_stream = 0;

static token_t * tokenize(const char *);
static void free_tokens(token_t *);
static void point_out_error(token_t *, int);
static void error(const char *, token_t *, int);

static token_t * tokenize(const char * str) {
    token_t * new_node = 0;
    token_t * prev_node = 0;
    token_t * first = 0;
    
    char * line = (char *) str;
    
    char buf[CMD_BUF_SIZE];
    
    size_t index;
    
    int next = 0;
    int done = 0;
    int iter;
    size_t iter_st;
    
    assert(*str);
    
    while (!done) {
        if (isspace(*line)) {
            ++line;
            continue;
        }
        
        if (!new_node) {
            new_node = (token_t *) malloc(sizeof(token_t));
            
            new_node->type = TOKEN_NONE;
            next = 0;
            new_node->data = 0;
            new_node->prev = 0;
            new_node->next = 0;
            
            memset(buf, 0, sizeof(char) * CMD_BUF_SIZE);
            
            if (prev_node) {
                new_node->prev = prev_node;
                prev_node->next = new_node;
            } else {
                prev_node = new_node;
            }
        }
        
        if (!first)
            first = new_node;
        
        for (iter = 0; iter < NUM_SYMBOLS; ++iter) {
            if (*line == symbols[iter]) {
                new_node->type = symbol_ids[iter];
                next = 1;
                ++line;
                break;
            }
        }
        
        if (!next) {
            index = 0;
            if (isdigit(*line)) {
                while (isdigit(*line))
                    buf[index++] = *line++;
                    
                buf[index] = 0;
                next = new_node->type = NUMBER;
                new_node->data = malloc(sizeof(size_t));
                memset(new_node->data, 0, sizeof(size_t));
                iter_st = atol(buf);
                memcpy(new_node->data, &iter_st, sizeof(size_t));
            } else {
                while (isalpha(*line))
                    buf[index++] = *line++;
                
                buf[index] = 0;
                
                for (iter = 0; iter < NUM_KEYWORDS; ++iter) {
                    if (streq(buf, keyword_strs[iter])) {
                        new_node->type = keyword_ids[iter];
                        next = 1;
                        break;
                    }
                }
                
                if (!next) {
                    new_node->type = COMMAND;
                    while (strlen(line) > 0)
                        buf[index++] = *line++;
                    
                    buf[index] = 0;
                    
                    if (!index) {
                        new_node->type = TOKEN_NONE;
                    } else {
                        new_node->data = malloc(strlen(buf));
                        memcpy(new_node->data, buf, index + 1);
                    }
                    
                    done = 1;
                }
            }
        }
        
        if (next) {
            prev_node = new_node;
            new_node = 0;
        }
        
    }
    
    return first;
}

static void free_tokens(token_t * head) {
    token_t * tmp;
    int i = 0;
    while (head) {
        if (head->data)
            free(head->data);
        tmp = head->next;
        free(head);
        head = tmp;
        ++i;
    }
    head = 0;
}

static void point_out_error(token_t * head, int err_tok) {
    char buf[CMD_BUF_SIZE];
    char arr[CMD_BUF_SIZE];
    
    int count = 0;
    int index = 0;
    int iter_c = 0;
    
    memset(buf, 0, CMD_BUF_SIZE);
    memset(buf, 0, CMD_BUF_SIZE);
    
    do {
        ++iter_c;
        
        if (iter_c == err_tok)
            index = count;
        
        assert(count < CMD_BUF_SIZE);
        
#define CASE(cond, fmt, str) \
    case cond: count += sprintf(&buf[count], fmt, str); break;
        switch (head->type) {
        CASE(LUA,           "%s", "lua")
        CASE(CONTROL,       "%s", "control")
        CASE(SYSTEM,        "%s", "system")
        CASE(ASYNC,         "%s", "async")
        CASE(STORE,         "%s", "store")
        CASE(AFTER,         "%s", "after")
        CASE(TIMES,         "%s", "times")
        CASE(TARGET,        "%s", "target")
        CASE(CALLBACK,      "%s", "callback")
        CASE(COMMAND,       "%s", (char *) head->data)
        CASE(RIGHT_BRACKET, "%s", "(")
        CASE(LEFT_BRACKET,  "%s", ")")
        CASE(NUMBER,        "%i", *((int *) head->data))
        CASE(COMMA,         "%s", ",")
        CASE(EQUALS,        "%s", "=")
        default: break;
        }
#undef CASE

    } while ((head = head->next));
    
    if (index > -1) {
        memset(arr, ' ', index);
        arr[index] = '^';
        arr[index + 1] = 0;
        
        if (error_stream)
            fprintf(error_stream, "\t%s\n\t%s\n", buf, arr);
    } else {
        if (error_stream)
            fprintf(error_stream, "%s\n", buf);
    }
}

static void error(const char * msg, token_t * first, int token) {
    if (error_stream) {
        fprintf(error_stream, "syntax error: token %i %s\n", token, msg);
        point_out_error(first, token);
    }
}

void parser_set_error_stream(FILE * stream) {
    error_stream = stream;
}

void parse(const char * str, cmd_t * cmd) {
    token_t * head;
    token_t * first;
    
    int count = 0;
    int syntax_ok = 1;
    
    int ASYNC_set, STORE_set, AFTER_set, TIMES_set, TARGET_set, CALLBACK_set;
    ASYNC_set = STORE_set = AFTER_set = TIMES_set = TARGET_set = CALLBACK_set = 0;
    
    assert(cmd);
    
    head = tokenize(str);
    first = head;
    
    do {
        ++count;
        
#define CASE(cond) case cond: \
    if (cond ## _set) error(DUPLICATE_PARAMETER, first, count); \
    cond ## _set = 1; break;
        switch (head->type) {
        CASE(ASYNC);
        CASE(STORE)
        CASE(AFTER)
        CASE(TIMES)
        CASE(TARGET)
        CASE(CALLBACK)
        default:
            break;
        }
#undef CASE
        
        switch (head->type) {
        case LUA:
        case CONTROL:
        case SYSTEM:
            if (head->prev) {
                error(MUST_BEGIN_WITH_TOKEN, first, count);
                syntax_ok = 0;
                break;
            }
            
            if (!head->next) {
                error(MUST_NOT_BE_LAST_TOKEN, first, count);
                syntax_ok = 0;
                break;
            }
            
            switch (head->next->type) {
                case RIGHT_BRACKET:
                    break;
                default:
                    error(MUST_BE_FOLLOWED_BY_RIGHT_BRACKET, first, count);
                    syntax_ok = 0;
                    break;
            }
            break;
        default:
            if (!head->prev) {
                error(MUST_BEGIN_WITH_TOKEN, first, count);
                syntax_ok = 0;
            }
            break;
        }
        
        switch (head->type) {
        case AFTER:
        case TIMES:
        case TARGET:
        case CALLBACK:
            switch (head->next->type) {
            case EQUALS:
                break;
            default:
                error(MUST_BE_FOLLOWED_BY_EQUALS, first, count);
                syntax_ok = 0;
                break;
            }
            break;
        case STORE:
        case ASYNC:
        case NUMBER:
            switch (head->next->type) {
            case COMMA:
            case LEFT_BRACKET:
                break;
            default:
                error(MUST_BE_FOLLOWED_BY_COMMA_CLOSING_BRACKET, first, count);
                syntax_ok = 0;
                break;
            }
            break;
        default:
            break;
        }
        
        switch (head->type) {
        case COMMA:
            switch (head->next->type) {
            case AFTER:
            case TIMES:
            case CALLBACK:
            case STORE:
            case ASYNC:
            case TARGET:
                break;
            default:
                error(MUST_BE_FOLLOWED_BY_ARGS, first, count);
                syntax_ok = 0;
                break;
            }
        case RIGHT_BRACKET:
            switch (head->next->type) {
            case LEFT_BRACKET:
            case AFTER:
            case TIMES:
            case CALLBACK:
            case STORE:
            case ASYNC:
            case TARGET:
                break;
            default:
                error(MUST_BE_FOLLOWED_BY_LEFT_BRACKET_OR_ARGS, first, count);
                syntax_ok = 0;
                break;
            }
            break;
        case LEFT_BRACKET:
            switch (head->next->type) {
            case COMMAND:
                break;
            default:
                error(MUST_BE_FOLLOWED_BY_COMMAND, first, count);
                syntax_ok = 0;
                break;
            }
            break;
        case EQUALS:
            switch (head->next->type) {
            case NUMBER:
                break;
            default:
                error(MUST_BE_FOLLOWED_BY_NUMBER, first, count);
                syntax_ok = 0;
                break;
            }
        default:
            break;
        }
        
        if (head->type == COMMAND)
            break;
    } while ((head = head->next));
    
    head = first;
    
    if (syntax_ok) {
        do {
        
#define CASE(cond, stmt) case cond: stmt; break;
            switch (head->type) {
            CASE(LUA,      cmd->type     = CMD_LUA)
            CASE(CONTROL,  cmd->type     = CMD_CONTROL)
            CASE(SYSTEM,   cmd->type     = CMD_SYSTEM)
            CASE(ASYNC,    cmd->async    = 1)
            CASE(STORE,    cmd->store    = 1)
            CASE(AFTER,    cmd->after    = *((size_t *) head->next->next->data))
            CASE(TIMES,    cmd->times    = *((size_t *) head->next->next->data))
            CASE(TARGET,   cmd->target   = *((size_t *) head->next->next->data))
            CASE(CALLBACK, cmd->callback = *((size_t *) head->next->next->data))
            case COMMAND:
                /* can't just assign head->data since it will get free()'d later */
                cmd->cmd_str = (char *) malloc(strlen((char *) head->data));
                strcpy(cmd->cmd_str, (char *) head->data);
                break;
            default:
                break;
            }
#undef CASE

        } while ((head = head->next));
        cmd->error = CMD_ERROR_NONE;
    } else if (error_stream) {
        fprintf(error_stream, "syntax check failed\n");
        cmd->error = CMD_SYNTAX_ERROR;
    }
    
    free_tokens(first);
}
