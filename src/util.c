#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>

#include "util.h"

typedef struct runner_args {
    int sec;
    int times;
    funcptr func;
    exec_handle_t * handle;
    void * args;
} runner_args_t;

static void * runner(void *);

static void * runner(void * args) {
    runner_args_t ra;
    int i, willbreak;
    
    assert(args);
    
    ra = *((runner_args_t *) args);
    
    for (i = 0; i < ra.times || ra.times == 0; ++i) {
        if ((willbreak = pthread_mutex_lock(&ra.handle->mutex)))
            ra.handle->error = MUTEX_COULD_NOT_LOCK;
        
        if (ra.handle->status == THREAD_STOPPED)
            willbreak = 1;
            
        if ((willbreak = pthread_mutex_unlock(&ra.handle->mutex)))
            ra.handle->error = MUTEX_COULD_NOT_UNLOCK;
        
        if (willbreak)
            break;
        
        ra.func(ra.args);
        sleep(ra.sec);
    }
    
    pthread_mutex_destroy(&ra.handle->mutex);
    
    free(args);
    return 0;
}

int streq(const char * str1, const char * str2) {
    return strcmp(str1, str2) == 0;
}

void execute_after(int sec, funcptr func, void * args, exec_handle_t * handle) {
    execute_every(sec, 1, func, args, handle);
}

void execute_every(int sec, int times, funcptr func, void * args,
                   exec_handle_t * handle) {
    runner_args_t * ra = (runner_args_t *) malloc(sizeof(runner_args_t));
    
    assert(sec >= 0);
    assert(times > 0);
    assert(func);
        
    ra->sec = sec;
    ra->times = times;
    ra->func = func;
    ra->args = args;
    ra->handle = handle;
    
    pthread_mutex_init(&handle->mutex, 0);
    
    if (pthread_create(&handle->thread, 0, runner, (void *) ra))
        handle->error = THREAD_CREATE_ERROR;
}

void stop_execution(exec_handle_t * handle) {
    assert(handle);
    
    MUTEX_LOCK(handle->mutex, handle->error);
    
    handle->status = THREAD_STOPPED;
    
    MUTEX_UNLOCK(handle->mutex, handle->error);
}
