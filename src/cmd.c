#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "cmd.h"
#include "util.h"
#include "hash.h"
#include "parser.h"
#include "shell.h"
#include "system_cmd.h"
#include "control_cmd.h"
#include "lua_cmd.h"

typedef void (* executer_t)(cmd_t *);

cmd_t * cmd_create() {
    cmd_t * cmd = (cmd_t *) malloc(sizeof(cmd_t));
    
    cmd->cmd_str = 0;
    cmd->ret = 0;
    cmd->type = CMD_NONE;
    cmd->async = 0;
    cmd->store = 0;
    cmd->after = 0;
    cmd->times = 0;
    cmd->callback = 0;
    cmd->id = hash_put(CMD_HASH_TYPE, cmd);
    cmd->error = CMD_ERROR_NONE;
    cmd->ran = 0;
    
    cmd->handle = (exec_handle_t *) malloc(sizeof(exec_handle_t));
    
    return cmd;
}

void cmd_destroy(cmd_t * cmd) {
    if (cmd->ret) {
        if (cmd->ret->data) {
            free(cmd->ret->data);
        }
        free(cmd->ret);
    }

    if (cmd->cmd_str) {
        free(cmd->cmd_str);
    }

    free(cmd->handle);

    hash_del(cmd->id);
    
    free(cmd);
    cmd = 0;
}

size_t cmd_len(const cmd_t * cmd) {
    return sizeof(unsigned long) * 2 + strlen(cmd->cmd_str) + 8;
}

void cmd_dump(const cmd_t * cmd, char * buf) {
    size_t uls = sizeof(unsigned long);
    size_t sl = strlen(cmd->cmd_str) + 1;
    
    int callback = htonl(cmd->callback),
        id = htonl(cmd->id);
    
    memcpy(buf, &cmd->type, 1);
    memcpy(&buf[1], &cmd->async, 1);
    memcpy(&buf[2], &cmd->store, 1);
    memcpy(&buf[3], &cmd->after, 1);
    memcpy(&buf[4], &cmd->times, 1);
    memcpy(&buf[5], &cmd->error, 1);
    memcpy(&buf[6], &cmd->ran, 1);
    memcpy(&buf[7], &callback, uls);
    memcpy(&buf[7 + uls], &id, uls);
    memcpy(&buf[7 + 2 * uls], cmd->cmd_str, sl);
}

void cmd_load(cmd_t * cmd, const char * buf) {
    size_t uls = sizeof(unsigned long);
    
    unsigned long callback, id;
    
    memcpy(&cmd->type, buf, 1);
    memcpy(&cmd->async, &buf[1], 1);
    memcpy(&cmd->store, &buf[2], 1);
    memcpy(&cmd->after, &buf[3], 1);
    memcpy(&cmd->times, &buf[4], 1);
    memcpy(&cmd->error, &buf[5], 1);
    memcpy(&cmd->ran, &buf[6], 1);
    memcpy(&callback, &buf[7], uls);
    memcpy(&id, &buf[7 + uls], uls);
    
    cmd->cmd_str = (char *) &buf[7 + 2 * uls];
    
    cmd->callback = ntohl(callback);
    cmd->id = ntohl(id);
}

void cmd_pretty_print(char * buf, const cmd_t * cmd) {
    size_t count = 0;
    
#define APPEND(fmt, params) count += sprintf(&buf[count], fmt, params)
#define _APPEND(fmt) count += sprintf(&buf[count], fmt)
    
    APPEND("Command (%p):\n", (void *) cmd);
    
    switch (cmd->type) {
    case CMD_LUA:     _APPEND("\ttype: lua\n");     break;
    case CMD_CONTROL: _APPEND("\ttype: control\n"); break;
    case CMD_SYSTEM:  _APPEND("\ttype: system\n");  break;
    default:          _APPEND("\ttype: unknown\n"); break;
    }
    
    if (cmd->async) _APPEND("\tasync: yes\n");
    else _APPEND("\tasync: no\n");
    
    if (cmd->store) _APPEND("\tstore: yes\n");
    else _APPEND("\tstore: no\n");
    
    APPEND("\tafter: %lu\n", cmd->after);
    APPEND("\ttimes: %lu\n", cmd->times);
    
    switch (cmd->error) {
    case CMD_FAILED:
        _APPEND("\terror: command failed\n");
        break;
    case CMD_TOO_LONG:
        _APPEND("\terror: command is too long\n");
        break;
    case CMD_CANNOT_RECURSE:
        _APPEND("\terror: command cannot recurse\n");
        break;
    default:
        _APPEND("\terror: no error\n");
        break;
    }
    
    if (cmd->ran) _APPEND("\tran: yes\n");
    else _APPEND("\tran: no\n");
    
    APPEND("\thandle: %p\n", (void *) cmd->handle);
    APPEND("\tcallback: %lu\n", cmd->callback);
    APPEND("\tid: %lu\n", cmd->id);
    APPEND("\tcmd_str: \"%s\"\n", cmd->cmd_str);
    
#undef APPEND
#undef _APPEND
    
    buf[count] = 0;
}

static void * cmd_wrap_lua_exec(void * arg) {
    lua_cmd_exec((cmd_t *) arg);
    return 0;
}

static void * cmd_wrap_system_exec(void * arg) {
    system_cmd_exec((cmd_t *) arg);
    return 0;
}

static void * cmd_wrap_control_exec(void * arg) {
    control_cmd_exec((cmd_t *) arg);
    return 0;
}

void cmd_exec(const char * cmd_str, shell_t * shell) {
    cmd_t * cmd = cmd_create();
    funcptr executer = 0;
    
    parser_set_error_stream(shell->o_stream);

    parse(cmd_str, cmd);  

    if (cmd->error) {
        /* FIXME: this needs to handle the syntax error case to avoid running the null executer */
        fprintf(shell->o_stream, "");
    }

    switch (cmd->type) {
    case CMD_LUA:
        executer = cmd_wrap_lua_exec;
        break;
    case CMD_SYSTEM:
        executer = cmd_wrap_system_exec;
        break;
    case CMD_CONTROL:
        executer = cmd_wrap_control_exec;
        break;
    case CMD_NONE:
        break;
    }
    
    if (cmd->async) {
        if (cmd->times) {
            execute_every(cmd->after, cmd->times, executer, cmd, cmd->handle);
        } else if (cmd->after) {
            execute_after(cmd->after, executer, cmd, cmd->handle);
        } else {
            execute_after(0, executer, cmd, cmd->handle);
        }
    } else {
        if (cmd->times) {
            while (cmd->times --> 0) {
                sleep(cmd->after);
                executer(cmd);
            }
        } else if (cmd->after) {
            sleep(cmd->after);
            executer(cmd);
        } else {
            executer(cmd);
        }
    }

    /* done */

    /* this should be done in a callback, since async */
    cmd_destroy(cmd);
}
